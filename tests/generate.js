const fs = require('fs');
const path = require('path');
const chai = require('chai');
const dirtyChai = require('dirty-chai');
const dummyLocalisation = require('../');

const expect = chai.expect;
chai.use(dirtyChai);

const SOURCE_PATH = 'tests/tmp/temp_file_test.json';
const DESTINATION_PATH = 'tests/tmp/temp_file_2_test.json';

const PSEUDO_CONTENT = "XXX";

afterEach(() => {
  try { fs.unlinkSync(SOURCE_PATH); fs.unlinkSync(DESTINATION_PATH) } catch (e) {}
});

describe('generate', () => {
  before(() => {
    fs.mkdir('tests/tmp', (err) => {});
  });

  after(() => {
    try { fs.rmdirSync('tests/tmp') } catch (e) {}
  });

  it('generates a new file at DESTINATION_PATH based on the contents of the file at SOURCE_PATH', () => {
    const givenData = { 'button.cancel.label': 'Cancel', 'button.save.label': 'Save' };
    const expectedData = { 'button.cancel.label': `${PSEUDO_CONTENT}Cancel${PSEUDO_CONTENT}`, 'button.save.label': `${PSEUDO_CONTENT}Save${PSEUDO_CONTENT}`};
    fs.writeFileSync(SOURCE_PATH, JSON.stringify(givenData), 'utf8');
    dummyLocalisation.generate(SOURCE_PATH, DESTINATION_PATH, PSEUDO_CONTENT);

    // parse then stringify to remove white space issues
    expect(JSON.stringify(JSON.parse(fs.readFileSync(DESTINATION_PATH, 'utf8')))).to.equal(JSON.stringify(expectedData));
  });

  it('overwrites the contents of the file at DESTINATION_PATH based on the contents of the file at SOURCE_PATH', () => {
    const givenData = { 'dialog.title': 'Unsaved Changes', 'dialog.message': 'Are you sure you want to exit?' };
    const expectedData = { 'dialog.title': `${PSEUDO_CONTENT}Unsaved Changes${PSEUDO_CONTENT}`, 'dialog.message': `${PSEUDO_CONTENT}Are you sure you want to exit?${PSEUDO_CONTENT}` };
    fs.writeFileSync(SOURCE_PATH, JSON.stringify(givenData), 'utf8');
    fs.writeFileSync(DESTINATION_PATH, '', 'utf8');
    dummyLocalisation.generate(SOURCE_PATH, DESTINATION_PATH, PSEUDO_CONTENT);

    // parse then stringify to remove white space issues
    expect(JSON.stringify(JSON.parse(fs.readFileSync(DESTINATION_PATH, 'utf8')))).to.equal(JSON.stringify(expectedData));
  });

  it('adds new line and indentation at the DESTINATION_PATH', () => {
    const givenData = { 'button.cancel.label': 'Cancel', 'button.save.label': 'Save' };

    fs.writeFileSync(SOURCE_PATH, JSON.stringify(givenData), 'utf8');
    dummyLocalisation.generate(SOURCE_PATH, DESTINATION_PATH, PSEUDO_CONTENT);

    const result = fs.readFileSync(DESTINATION_PATH, 'utf8');
    expect(result).to.equal(`{\n  "button.cancel.label": "${PSEUDO_CONTENT}Cancel${PSEUDO_CONTENT}",\n  "button.save.label": "${PSEUDO_CONTENT}Save${PSEUDO_CONTENT}"\n}\n`);
    
  });
});